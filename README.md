
# timezone, Node command-line app

Node command-line app that provides timezone of a given zipcode.

Expects:
```sh
timezone <zipcode>
```
